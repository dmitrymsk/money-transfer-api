package com.sky.money.transfer.repo.impl;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.accounts.BankAccount;
import com.sky.money.transfer.domain.currencies.BaseCurrency;
import com.sky.money.transfer.domain.currencies.Currency;
import com.sky.money.transfer.repo.AccountsRepository;
import com.sky.money.transfer.utils.pagination.PaginatedResult;
import com.sky.money.transfer.utils.pagination.PaginatedResultImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.sky.money.transfer.config.ConfigStore.ACCOUNT_NUMBER_DIGITS_COUNT;
import static com.sky.money.transfer.config.ConfigStore.DEFAULT_INITIAL_BALANCE;

public class DefaultAccountsRepository implements AccountsRepository {

    private static final Logger logger = LoggerFactory.getLogger(DefaultAccountsRepository.class);
    private final AtomicLong counter;
    private final ConcurrentMap<Long, Account> accounts;


    DefaultAccountsRepository() {
        this.counter = new AtomicLong(0L);
        this.accounts = new ConcurrentHashMap<>();
    }

    @Override
    public Account getById(Long id) {
        return accounts.getOrDefault(id, BankAccount.createFakeAccount());
    }


    @Override
    public Account addAccountWithDefaultBalance(Currency currency, String holderName) {
        return addAccount(currency, holderName, BigDecimal.valueOf(DEFAULT_INITIAL_BALANCE));
    }

    @Override
    public Account addAccountWithDefaultCurrency(String holderName, BigDecimal initialBalance) {
        return addAccount(BaseCurrency.getDefault(), holderName, initialBalance);
    }


    @Override
    public int size() {
        return accounts.size();
    }


    @Override
    public PaginatedResult<Account> getAll(int pageNumber, int recordsPerPage) {
        return PaginatedResultImpl.from(pageNumber, recordsPerPage, accounts);
    }

    @Override
    public PaginatedResult<Account> findAccountsByHolderName(String holderName) {
        ConcurrentMap<Long, Account> accounts = this.accounts.values().stream()
                .filter(a -> a.getHolderName().contains(holderName))
                .sorted(Comparator.comparing(Account::getId)).collect(Collectors.toConcurrentMap(Account::getId, Function.identity()));
        return PaginatedResultImpl.from(1, 100, accounts);  //todo
    }

    @Override
    public Account addAccount(Currency currency, String holderName, BigDecimal initialBalance) {

        long id = counter.incrementAndGet();
        final Account account = BankAccount.createAccount(id, currency, generateAccountNumber(id), holderName, initialBalance);
        accounts.putIfAbsent(account.getId(), account);
        logger.trace("Add account to repo: {}", account);
        return account;
    }

    private String generateAccountNumber(final long id) {
        return ConfigStore.DEFAULT_BANK_SORT_CODE + " " + StringUtils.leftPad(String.valueOf(id), ACCOUNT_NUMBER_DIGITS_COUNT, '0');
    }


}
