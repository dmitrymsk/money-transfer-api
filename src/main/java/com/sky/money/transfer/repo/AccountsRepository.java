package com.sky.money.transfer.repo;

import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.currencies.Currency;
import com.sky.money.transfer.utils.pagination.PaginatedResult;

import java.math.BigDecimal;

public interface AccountsRepository extends Repository<Account> {

    PaginatedResult<Account> findAccountsByHolderName(String holderName);

    Account addAccount(Currency currency, String holderName, BigDecimal initialBalance);

    Account addAccountWithDefaultBalance(Currency currency, String holderName);

    Account addAccountWithDefaultCurrency(String holderName, BigDecimal initialBalance);
}
