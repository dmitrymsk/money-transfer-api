package com.sky.money.transfer.repo;

import com.sky.money.transfer.utils.pagination.Pageable;

public interface Repository<T> extends Pageable<T> {

    int size();

    T getById(Long id);

}
