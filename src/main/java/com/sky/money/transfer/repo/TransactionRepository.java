package com.sky.money.transfer.repo;

import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.transactions.Transaction;
import com.sky.money.transfer.utils.pagination.PaginatedResult;
import com.sky.money.transfer.utils.pagination.Pagination;

import java.math.BigDecimal;

public interface TransactionRepository extends Repository<Transaction> {

    Transaction add(Account debit, Account credit, BigDecimal amount);

    PaginatedResult<Transaction> getTransactionsForAccount(Account account, int pageNumber, int recordsPerPage);

    default PaginatedResult<Transaction> getTransactionsForAccount(Account account, Pagination pagination) {
        return getTransactionsForAccount(account, pagination.getPageNumber(), pagination.getRecordsPerPage());
    }
}
