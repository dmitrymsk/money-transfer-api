package com.sky.money.transfer.repo.impl;


import com.sky.money.transfer.repo.AccountsRepository;
import com.sky.money.transfer.repo.TransactionRepository;

public enum Context {

    INSTANCE(new DefaultAccountsRepository(), new DefaultTransactionRepository());

    private final AccountsRepository accountsRepo;
    private final TransactionRepository transactionRepo;

    Context(AccountsRepository accountsRepo,
            TransactionRepository transactionRepo) {
        this.transactionRepo = transactionRepo;
        this.accountsRepo = accountsRepo;

    }

    public AccountsRepository getAccountsRepository() {
        return this.accountsRepo;
    }

    public TransactionRepository getTransactionRepository() {
        return this.transactionRepo;
    }
}
