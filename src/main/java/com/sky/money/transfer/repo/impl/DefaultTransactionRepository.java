package com.sky.money.transfer.repo.impl;

import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.transactions.MoneyTransaction;
import com.sky.money.transfer.domain.transactions.Transaction;
import com.sky.money.transfer.repo.TransactionRepository;
import com.sky.money.transfer.utils.pagination.PaginatedResult;
import com.sky.money.transfer.utils.pagination.PaginatedResultImpl;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

final class DefaultTransactionRepository implements TransactionRepository {
    private static final Logger logger = LoggerFactory.getLogger(DefaultTransactionRepository.class);
    private final AtomicLong counter = new AtomicLong(0L);
    private final ConcurrentMap<Long, Transaction> transactions = new ConcurrentHashMap<>();

    @Override
    public Transaction getById(Long id) {
        return transactions.getOrDefault(id, MoneyTransaction.getFakeTransaction());
    }

    @Override
    public int size() {
        return transactions.size();
    }

    @Override
    public Transaction add(Account debit, Account credit, BigDecimal amount) {
        final var transaction = MoneyTransaction.make(counter.incrementAndGet(), debit, credit, amount);
        transactions.putIfAbsent(transaction.getId(), transaction);
        logger.trace("Add transaction to repo: {}", transaction);
        return transaction;
    }

    @Override
    public PaginatedResult<Transaction> getAll(int pageNumber, int recordsPerPage) {
        return PaginatedResultImpl.from(pageNumber, recordsPerPage, transactions);
    }

    @Override
    public PaginatedResult<Transaction> getTransactionsForAccount(@NonNull Account account, int pageNumber, int recordsPerPage) {
        Predicate<Transaction> predicate = t -> t.getDebit().equals(account) || t.getCredit().equals(account);
        return PaginatedResultImpl.from(pageNumber, recordsPerPage, transactions, predicate);
    }
}
