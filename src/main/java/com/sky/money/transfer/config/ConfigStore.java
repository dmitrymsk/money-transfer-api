package com.sky.money.transfer.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

public class ConfigStore {


    public static final String SERVER_HOST;
    public static final int SERVER_PORT;
    public static final int SERVER_THREAD_COUNT;
    public static final int TRANSACTION_WAITING_LOCK_TIME;
    public static final int TRANSACTION_MAX_TRY_ATTEMPTS;
    public static final int THREAD_POOL_SIZE;
    public static final String ACCOUNTS_URI;
    public static final String TRANSACTIONS_URI;
    public static final String API_VERSION;
    public static final String CONTENT_TYPE;
    public static final String DEFAULT_BANK_SORT_CODE;
    public static final Double DEFAULT_INITIAL_BALANCE;
    public static final int MIN_TRANSACTION_AMOUNT;
    public static final int MAX_TRANSACTION_AMOUNT;
    public static final int ACCOUNTS_SAMPLE_COUNT;
    public static final int TRANSACTIONS_SAMPLE_COUNT;
    public static final String DEFAULT_CURRENCY_ISO_CODE;
    public static final int ACCOUNT_NUMBER_DIGITS_COUNT;
    public static final String SLASH = "/";
    public static final String ID_PARAM = ":id";
    public static final String API_PREFIX = "api";
    public static final int FUTURE_AWAIT_PERIOD = 10; // in seconds
    public static final int TEST_COUNCURRENCY_NUM_OF_THREADS;
    public static final int TEST_COUNCURRENCY_NUM_OF_TRANSACTIONS;
    public static final int TEST_COUNCURRENCY_MAX_TEST_WAITING_TIME;

    static {
        Config config = ConfigFactory.systemProperties()
                .withFallback(ConfigFactory.parseFile(new File("myapp.conf")))
                .withFallback(ConfigFactory.parseResources("myapp.conf"));
        SERVER_HOST = config.getString("core.serverHost");
        SERVER_PORT = config.getInt("core.serverPort");
        SERVER_THREAD_COUNT = config.getInt("core.serverThreadCount");

        THREAD_POOL_SIZE = config.getInt("core.threadPoolSize");
        ACCOUNTS_URI = config.getString("core.accountsUri");
        TRANSACTIONS_URI = config.getString("core.transactionsUri");
        API_VERSION = config.getString("core.apiVersion");
        CONTENT_TYPE = config.getString("core.contentType");

        TRANSACTION_WAITING_LOCK_TIME = config.getInt("transactions.maxWaitingLockTime");
        TRANSACTION_MAX_TRY_ATTEMPTS = config.getInt("transactions.maxTryAttempts");

        DEFAULT_BANK_SORT_CODE = config.getString("sample.defaultSortCode");
        MIN_TRANSACTION_AMOUNT = config.getInt("sample.minTransactionAmount");
        MAX_TRANSACTION_AMOUNT = config.getInt("sample.maxTransactionAmount");
        DEFAULT_CURRENCY_ISO_CODE = config.getString("sample.defaultCurrencyIsoCode");
        ACCOUNTS_SAMPLE_COUNT = config.getInt("sample.accountsCount");
        TRANSACTIONS_SAMPLE_COUNT = config.getInt("sample.transactionsCount");
        DEFAULT_INITIAL_BALANCE = config.getDouble("sample.defaultInitialBalance");
        ACCOUNT_NUMBER_DIGITS_COUNT = config.getInt("sample.accountNumberDigitsCount");

        TEST_COUNCURRENCY_NUM_OF_THREADS = config.getInt("test.concurrency.numOfThreads");
        TEST_COUNCURRENCY_NUM_OF_TRANSACTIONS = config.getInt("test.concurrency.numOfTransactions");
        TEST_COUNCURRENCY_MAX_TEST_WAITING_TIME = config.getInt("test.concurrency.maxTestWaitingTime");

    }


}