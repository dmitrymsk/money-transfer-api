package com.sky.money.transfer.utils.generators;

import com.sky.money.transfer.repo.impl.Context;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.sky.money.transfer.config.ConfigStore.FUTURE_AWAIT_PERIOD;

abstract class CommonDataGenerator {

    static final Logger logger = LoggerFactory.getLogger(CommonDataGenerator.class);

    final AtomicInteger counter;
    final Context context;

    final Collection<Long> ids;
    final String message;
    final int threadPoolSize;

    CommonDataGenerator(@NonNull final Context context, final String message, final int threadPoolSize) {

        this.counter = new AtomicInteger(0);
        this.context = context;
        this.ids = new ConcurrentLinkedQueue<>();
        this.message = message;
        this.threadPoolSize = threadPoolSize;
    }

    CommonDataGenerator(final Context context, final String message) {
        this(context, message, Runtime.getRuntime().availableProcessors());
    }

    abstract List<Future<?>> doGenerate(final ExecutorService threadPool);

    final List<Long> generate() {
        final var timeStart = System.nanoTime();
        try {
            logger.info("Creating sample data {}", message);
            final var threadPool = Executors.newFixedThreadPool(threadPoolSize);
            final var futures = doGenerate(threadPool);
            threadPool.shutdown();
            waitForCompletion(futures);
        } finally {
            final var timeEnd = System.nanoTime();
            logger.info("Creating sample {} is finished. Time elapsed = {} mills", message, (timeEnd - timeStart) / 1_000_000);
        }
        return List.copyOf(ids);
    }

    private void waitForCompletion(final List<Future<?>> futures) {
        logger.debug("Waiting for completion from {} tasks:", futures.size());
        for (final var future : futures) {
            try {
                future.get(FUTURE_AWAIT_PERIOD, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                logger.error(e.getMessage(), e);

            }
        }
    }
}
