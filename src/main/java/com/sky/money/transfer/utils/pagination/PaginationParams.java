package com.sky.money.transfer.utils.pagination;

import spark.Request;

public final class PaginationParams implements Pagination {

    private final int recordsPerPage;
    private final int pageNumber;
    private final boolean valid;

    private PaginationParams(Request request) {
        int limit;
        int page;
        boolean valid = true;
        final var limitStr = request.queryParams("l");
        if (limitStr != null) {
            limit = Integer.valueOf(limitStr, 10);
        } else {
            limit = 100; //  by default
        }
        final var pageStr = request.queryParams("p");
        if (pageStr != null) {
            page = Integer.valueOf(pageStr, 10);
        } else {
            page = 1; // default
        }

        this.recordsPerPage = limit;
        this.pageNumber = page;
        this.valid = valid;
    }

    public static PaginationParams from(Request request) {
        final var pgParams = new PaginationParams(request);
        if (pgParams.isNotCorrect()) {
            throw new IllegalArgumentException("Invalid pagination parameters");
        }
        return pgParams;
    }

    @Override
    public int getRecordsPerPage() {
        return recordsPerPage;
    }

    @Override
    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public boolean isCorrect() {
        return valid;
    }

    public String toString() {
        return "PaginationParams(recordsPerPage=" + this.getRecordsPerPage() + ", pageNumber=" + this.getPageNumber() + ", valid=" + this.isCorrect() + ")";
    }
}
