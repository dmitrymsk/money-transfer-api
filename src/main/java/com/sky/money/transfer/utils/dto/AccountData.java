package com.sky.money.transfer.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class AccountData {
    @NonNull
    String holderName;
    BigDecimal initialBalance;

}
