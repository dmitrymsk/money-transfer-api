package com.sky.money.transfer.utils.generators;

import com.sky.money.transfer.repo.impl.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.sky.money.transfer.config.ConfigStore.THREAD_POOL_SIZE;
import static com.sky.money.transfer.config.ConfigStore.TRANSACTIONS_SAMPLE_COUNT;

public class DataGenerator {

    private static final Logger logger = LoggerFactory.getLogger(DataGenerator.class);

    private final Context context;

    private DataGenerator(Context context) {
        this.context = context;
    }

    public static DataGenerator getInstance(Context context) {
        return new DataGenerator(context);
    }

    public void generate() {
        try {
            final var accountIds = generateAccounts();
            generateClientTransactions(accountIds);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
        }
    }


    private List<Long> generateAccounts() {
        final var accountGenerator = new RandomAccountGenerator(context);
        final var accountIds = accountGenerator.generate();
        logger.debug("Account ids count = {}", accountIds.size());
        logger.debug("Account repository size = {}", context.getAccountsRepository().size());
        return accountIds;
    }


    private void generateClientTransactions(final List<Long> accountIds) {
        final var transactionGenerator = new RandomTransactionGenerator(
                context, accountIds, THREAD_POOL_SIZE, TRANSACTIONS_SAMPLE_COUNT);
        final var trnIds = transactionGenerator.generate();
        logger.debug("Transaction ids count = {}", trnIds.size());
        logger.debug("Transaction repository size = {}", context.getTransactionRepository().size());
    }
}
