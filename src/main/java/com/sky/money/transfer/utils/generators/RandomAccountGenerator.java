package com.sky.money.transfer.utils.generators;

import com.sky.money.transfer.domain.currencies.BaseCurrency;
import com.sky.money.transfer.repo.impl.Context;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static com.sky.money.transfer.config.ConfigStore.ACCOUNTS_SAMPLE_COUNT;

class RandomAccountGenerator extends CommonDataGenerator {

    RandomAccountGenerator(final Context context) {
        super(context, "accounts");
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>();
        IntStream.iterate(0, n -> n + 1)
                .takeWhile(n -> n < ACCOUNTS_SAMPLE_COUNT)
                .forEach((int n) ->
                {
                    Runnable runnableTask = this::generateAccount;
                    futures.add(threadPool.submit(runnableTask));
                });

        return futures;
    }

    private void generateAccount() {
        final var accountsRepository = context.getAccountsRepository();
        final var a = accountsRepository.addAccountWithDefaultBalance(BaseCurrency.getDefault(), getRandomString(10));
        ids.add(a.getId());

    }

    String getRandomString(int length) {
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers).toLowerCase();
    }
}
