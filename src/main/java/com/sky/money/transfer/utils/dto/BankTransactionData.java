package com.sky.money.transfer.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public final class BankTransactionData {
    private final Long debitAccountId;
    private final Long creditAccountId;
    private final BigDecimal amount;

}
