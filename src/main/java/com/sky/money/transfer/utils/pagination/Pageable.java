package com.sky.money.transfer.utils.pagination;

public interface Pageable<T> {

    PaginatedResult<T> getAll(int pageNumber, int recordsPerPage);

    default PaginatedResult<T> getAll(Pagination pagination) {
        return getAll(pagination.getPageNumber(), pagination.getRecordsPerPage());
    }
}
