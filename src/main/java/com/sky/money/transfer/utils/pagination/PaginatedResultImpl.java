package com.sky.money.transfer.utils.pagination;

import com.sky.money.transfer.domain.core.HasId;
import com.sky.money.transfer.utils.validators.Validator;
import lombok.NonNull;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PaginatedResultImpl<T> implements PaginatedResult<T> {

    private final boolean hasMore;
    private final int pageNumber;
    private final int recordsPerPage;
    private final Collection<T> content;

    private PaginatedResultImpl(final int pageNumber, final int recordsPerPage, @NonNull Deque<T> content) {
        Validator.validatePagination(pageNumber, recordsPerPage);
        Validator.validatePageableContentSize(content, recordsPerPage);

        this.pageNumber = pageNumber;
        this.recordsPerPage = recordsPerPage;
        this.hasMore = content.size() > recordsPerPage;
        if (this.hasMore) {
            content.removeLast();
        }
        this.content = Collections.unmodifiableCollection(content);
    }

    static <T> PaginatedResult<T> of(final int pageNumber, final int recordsPerPage, @NonNull final Deque<T> content) {
        Validator.validatePagination(pageNumber, recordsPerPage);
        return new PaginatedResultImpl<>(pageNumber, recordsPerPage, content);
    }

    public static <T extends HasId> PaginatedResult<T> from(final int pageNumber, final int recordsPerPage,
                                                            @NonNull final Map<Long, T> content) {
        Validator.validatePagination(pageNumber, recordsPerPage);
        final var pagedContent = content.values().stream()
                .sorted(Comparator.comparing(T::getId))
                .skip(recordsPerPage * (pageNumber - 1))
                .limit(recordsPerPage + 1)
                .collect(Collectors.toCollection(LinkedList::new));
        return PaginatedResultImpl.of(pageNumber, recordsPerPage, pagedContent);
    }

    public static <T extends HasId> PaginatedResult<T> from(final int pageNumber, final int recordsPerPage,
                                                            @NonNull final Map<Long, T> content, Predicate<T> predicate) {
        Validator.validatePagination(pageNumber, recordsPerPage);
        final var pagedContent = content.values().stream()
                .filter(predicate)
                .sorted(Comparator.comparing(T::getId))
                .skip(recordsPerPage * (pageNumber - 1))
                .limit(recordsPerPage + 1)
                .collect(Collectors.toCollection(LinkedList::new));
        return PaginatedResultImpl.of(pageNumber, recordsPerPage, pagedContent);
    }

    @Override
    public boolean hasMorePages() {
        return hasMore;
    }

    @Override
    public Collection<T> getContent() {
        return content;
    }

    @Override
    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public int getRowsPerPage() {
        return recordsPerPage;
    }

    public String toString() {
        return "PaginatedResultImpl(hasMore=" + this.hasMore + ", pageNumber=" + this.getPageNumber() + ", recordsPerPage=" + this.getRowsPerPage() + ", content=" + this.getContent() + ")";
    }
}
