package com.sky.money.transfer.utils.pagination;

import com.sky.money.transfer.domain.core.Checkable;

public interface Pagination extends Checkable {

    int getRecordsPerPage();

    int getPageNumber();
}
