package com.sky.money.transfer.utils;

import com.google.gson.Gson;

public final class JsonUtils {

    private JsonUtils() {
    }

    public static Gson make() {
        return new Gson().newBuilder().setPrettyPrinting().create();
    }

    public static String toJson(Exception err, int errCode) {
        final ErrorInfo info = new ErrorInfo(err, errCode);
        return make().toJson(info);
    }

    private static class ErrorInfo {

        private final int errorCode;
        private final String errorMessage;

        ErrorInfo(Exception err, int errCode) {
            this.errorCode = errCode;
            this.errorMessage = err.getLocalizedMessage();
        }

        public int getErrorCode() {
            return this.errorCode;
        }

        public String getErrorMessage() {
            return this.errorMessage;
        }

        public String toString() {
            return "JsonUtils.ErrorInfo(errorCode=" + this.getErrorCode() + ", errorMessage=" + this.getErrorMessage() + ")";
        }
    }
}
