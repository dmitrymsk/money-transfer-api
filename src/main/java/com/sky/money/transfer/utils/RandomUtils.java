package com.sky.money.transfer.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class RandomUtils {

    private static final Logger logger = LoggerFactory.getLogger(RandomUtils.class);

    private RandomUtils() {
    }

    public static Pair<Long, Long> getRandomAccountIds(final List<Long> accountIds) {
        final int debitIdx = getRandom().nextInt(accountIds.size());
        final int creditIdx = debitIdx != 0 ? debitIdx - 1 : debitIdx + 1;
        return Pair.of(accountIds.get(debitIdx), accountIds.get(creditIdx));
    }


    private static Random getRandom() {
        return ThreadLocalRandom.current();
    }

    public static BigDecimal generateAmount(int min, int max) {
        return BigDecimal.valueOf(min + getRandom().nextInt(max - min), 2);
    }
}
