package com.sky.money.transfer.utils.generators;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.repo.AccountsRepository;
import com.sky.money.transfer.repo.impl.Context;
import com.sky.money.transfer.utils.RandomUtils;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class RandomTransactionGenerator extends CommonDataGenerator {

    private final int trnCount;
    private final List<Long> accountIds;

    RandomTransactionGenerator(final Context context, @NonNull final List<Long> accountIds,
                               final int threadPoolSize, int trnCount) {
        super(context, "clients transactions", threadPoolSize);

        this.accountIds = accountIds;
        this.trnCount = trnCount;
    }

    @Override
    List<Future<?>> doGenerate(final ExecutorService threadPool) {
        final List<Future<?>> futures = new ArrayList<>(trnCount);
        for (var i = 0; i < trnCount; ++i) {
            futures.add(threadPool.submit(this::generateTransaction));
        }
        return futures;
    }

    private void generateTransaction() {
        final AccountsRepository accountsRepository = context.getAccountsRepository();
        final Pair<Long, Long> randomIds = RandomUtils.getRandomAccountIds(accountIds);
        final Account debit = accountsRepository.getById(randomIds.getLeft());
        if (debit.isCorrect()) {
            final Account credit = accountsRepository.getById(randomIds.getRight());
            if (credit.isCorrect()) {
                final var amount = RandomUtils.generateAmount(ConfigStore.MIN_TRANSACTION_AMOUNT, ConfigStore.MAX_TRANSACTION_AMOUNT);
                final var transaction = context.getTransactionRepository().add(debit, credit, amount);
                transaction.run();
                ids.add(transaction.getId());
            } else {
                logger.error("Credit account with id = {} not found", randomIds.getRight());
            }
        } else {
            logger.error("Debit account with id = {} not found", randomIds.getLeft());
        }
    }
}
