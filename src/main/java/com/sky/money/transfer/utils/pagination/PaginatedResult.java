package com.sky.money.transfer.utils.pagination;

import java.util.Collection;

public interface PaginatedResult<T> {

    boolean hasMorePages();

    Collection<T> getContent();

    int getPageNumber();

    int getRowsPerPage();
}
