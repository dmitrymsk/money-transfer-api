package com.sky.money.transfer.service;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.transactions.Transaction;
import com.sky.money.transfer.domain.transactions.TransactionState;
import com.sky.money.transfer.repo.AccountsRepository;
import com.sky.money.transfer.repo.TransactionRepository;
import com.sky.money.transfer.repo.impl.Context;
import com.sky.money.transfer.utils.dto.BankTransactionData;
import com.sky.money.transfer.utils.generators.DataGenerator;
import com.sky.money.transfer.utils.pagination.PaginatedResult;
import com.sky.money.transfer.utils.pagination.PaginationParams;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

@Slf4j
public class DefaultBankService implements BankService {


    private final AccountsRepository accountsRepo;
    private final TransactionRepository transactionRepo;

    DefaultBankService() {
        final var context = Context.INSTANCE;
        accountsRepo = context.getAccountsRepository();
        transactionRepo = context.getTransactionRepository();
    }

    public static DefaultBankService getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public Transaction createTransaction(@NonNull BankTransactionData transactionData) {
        final var debit = accountsRepo.getById(transactionData.getDebitAccountId());
        validateAccount(debit, transactionData.getDebitAccountId());
        final var credit = accountsRepo.getById(transactionData.getCreditAccountId());
        validateAccount(credit, transactionData.getCreditAccountId());
        final var transaction = transactionRepo.add(debit, credit, transactionData.getAmount());
        int currentAttempt = 0;
        while (!transaction.run() && currentAttempt < ConfigStore.TRANSACTION_MAX_TRY_ATTEMPTS) {
            if (TransactionState.LOW_FUNDS.equals(transaction.getState())) {
                break;
            }
            log.debug("Try to repeat transaction, attempt no.: {}", ++currentAttempt);
            try {
                Thread.sleep(ConfigStore.TRANSACTION_WAITING_LOCK_TIME);
            } catch (InterruptedException e) {
                log.warn(e.getMessage(), e);
            }
        }
        return transaction;
    }

    @Override
    public PaginatedResult<Transaction> getTransactionsByAccount(@NonNull Long accountId, @NonNull PaginationParams params) {
        final var account = accountsRepo.getById(accountId);
        validateAccount(account, accountId);
        return transactionRepo.getTransactionsForAccount(account, params);

    }

    @Override
    public Transaction getTransactionById(@NonNull Long id) {
        Transaction transaction = transactionRepo.getById(id);
        if (transaction.isNotCorrect()) {
            throw new NoSuchElementException(String.format("Transaction with id %d is not found", id));
        }
        return transaction;
    }

    @Override
    public PaginatedResult<Transaction> getAllTransactions(@NonNull PaginationParams pgParams) {
        return transactionRepo.getAll(pgParams);
    }

    @Override
    public Account createAccount(@NonNull String holderName, BigDecimal initialBalance) {
        return accountsRepo.addAccountWithDefaultCurrency(holderName, initialBalance);

    }

    @Override
    public PaginatedResult<Account> findAccountsByAccountNumber(@NonNull String accountNumber) {
        throw new UnsupportedOperationException("Not implemented"); //todo
    }

    @Override
    public PaginatedResult<Account> findAccountsByHolderName(@NonNull String holderName) {
        return accountsRepo.findAccountsByHolderName(holderName);
    }

    @Override
    public Account getAccountById(@NonNull Long id) {
        Account account = accountsRepo.getById(id);
        if (account.isNotCorrect()) {
            throw new NoSuchElementException(String.format("Account with id %d is not found", id));
        }
        return account;
    }

    @Override
    public PaginatedResult<Account> getAllAccounts(@NonNull PaginationParams pgParams) {
        return accountsRepo.getAll(pgParams);
    }

    public void generateSampleData() {
        DataGenerator.getInstance(Context.INSTANCE).generate();
    }

    private void validateAccount(@NonNull Account account, @NonNull Long id) {
        if (account.isNotCorrect()) {
            throw new NoSuchElementException(String.format("Account with id %d is not found", id));
        }
    }

    private static class SingletonHolder {
        private static final DefaultBankService INSTANCE = new DefaultBankService();
    }
}
