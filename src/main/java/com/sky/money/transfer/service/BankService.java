package com.sky.money.transfer.service;

import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.transactions.Transaction;
import com.sky.money.transfer.utils.dto.BankTransactionData;
import com.sky.money.transfer.utils.pagination.PaginatedResult;
import com.sky.money.transfer.utils.pagination.PaginationParams;
import lombok.NonNull;

import java.math.BigDecimal;


public interface BankService {

    Transaction createTransaction(BankTransactionData data);

    PaginatedResult<Transaction> getTransactionsByAccount(Long accountId, PaginationParams params);

    Transaction getTransactionById(Long id);

    PaginatedResult<Transaction> getAllTransactions(PaginationParams pgParams);

    Account createAccount(@NonNull String holderName, BigDecimal initialBalance);

    PaginatedResult<Account> findAccountsByAccountNumber(String accountNumber);

    PaginatedResult<Account> findAccountsByHolderName(String holderName);

    Account getAccountById(Long id);

    PaginatedResult<Account> getAllAccounts(PaginationParams pgParams);

    void generateSampleData();


}
