package com.sky.money.transfer.domain.transactions;

import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.core.HasId;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface Transaction extends HasId {

    Account getDebit();

    Account getCredit();

    LocalDateTime getCreatedTime();

    LocalDateTime getLastModifiedTime();

    BigDecimal getAmount();

    TransactionState getState();

    boolean run();


}
