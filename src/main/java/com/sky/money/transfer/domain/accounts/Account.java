package com.sky.money.transfer.domain.accounts;

import com.sky.money.transfer.domain.core.HasId;
import com.sky.money.transfer.domain.currencies.Currency;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;

public interface Account extends HasId {

    String getFullAccountNumber();

    Currency getCurrency();

    BigDecimal getBalance();

    boolean debit(BigDecimal amount);

    boolean credit(BigDecimal amount);

    String getHolderName();

    Lock writeLock();


}
