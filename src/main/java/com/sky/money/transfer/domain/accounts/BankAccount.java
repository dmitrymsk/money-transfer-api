package com.sky.money.transfer.domain.accounts;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.currencies.BaseCurrency;
import com.sky.money.transfer.domain.currencies.Currency;
import com.sky.money.transfer.utils.validators.Validator;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount implements Account {

    private static final Logger logger = LoggerFactory.getLogger(BankAccount.class);

    private final Long id;
    private final String currencyCode;
    private final transient Currency currency;
    private final String fullAccountNumber;
    private final String holderName;
    private final transient Lock lock;
    private BigDecimal balance;

    private BankAccount(@NonNull Long id, @NonNull Currency currency, @NonNull String fullAccountNumber,
                        @NonNull String holderName, BigDecimal balance) {

        Validator.validateAmountNotNegative(balance);
        Validator.validateCurrencyCode(currency.getISOCode());

        this.id = id;
        this.currency = currency;
        this.currencyCode = currency.getISOCode();
        this.fullAccountNumber = fullAccountNumber;
        this.holderName = holderName;
        this.balance = balance;
        this.lock = new ReentrantLock();
    }


    public static Account createAccount(Long id, Currency currency, String number, String holderName, BigDecimal balance) {
        return new BankAccount(id, currency, number, holderName, balance);
    }

    public static Account createFakeAccount() {
        return new BankAccount(FAKE_ID, BaseCurrency.getDefault(), String.valueOf(FAKE_ID), "fakeHolder", BigDecimal.ZERO);
    }


    @Override
    public final Long getId() {
        return id;
    }

    @Override
    public final String getFullAccountNumber() {
        return fullAccountNumber;
    }

    @Override
    public final Currency getCurrency() {
        return currency;
    }


    @Override
    public final String getHolderName() {
        return holderName;
    }


    @Override
    public Lock writeLock() {
        return lock;
    }


    @Override
    public final BigDecimal getBalance() {
        try {
            if (lock != null)
                lock.lock();
            return balance;
        } finally {
            if (lock != null)
                lock.unlock();
        }
    }

    @Override
    public boolean debit(@NonNull BigDecimal amount) {
        Validator.validateAmountNotNegative(amount);
        try {
            if (lock.tryLock(ConfigStore.TRANSACTION_WAITING_LOCK_TIME, TimeUnit.MILLISECONDS)) {
                try {
                    if (balance.compareTo(amount) >= 0) {
                        balance = balance.subtract(amount);
                        return true;
                    }
                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            logger.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    @Override
    public boolean credit(@NonNull BigDecimal amount) {
        Validator.validateAmountNotNegative(amount);
        try {
            if (lock.tryLock(ConfigStore.TRANSACTION_WAITING_LOCK_TIME, TimeUnit.MILLISECONDS)) {
                try {
                    balance = balance.add(amount);
                } finally {
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            logger.error(e.getLocalizedMessage(), e);
        }
        return true;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankAccount{");
        sb.append("id=").append(id);
        sb.append(", currencyCode=").append(currency.getISOCode());
        sb.append(", fullAccountNumber='").append(fullAccountNumber).append('\'');
        sb.append(", holderName='").append(holderName).append('\'');
        sb.append(", lock=").append(lock);
        sb.append(", balance=").append(balance);
        sb.append('}');
        return sb.toString();
    }
}
