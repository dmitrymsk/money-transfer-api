package com.sky.money.transfer.domain.transactions;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.utils.validators.Validator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MoneyTransaction implements Transaction {


    private final Long id;
    private Account debit;
    private Account credit;
    private BigDecimal amount;
    private TransactionState state;
    private LocalDateTime createdTime;
    private LocalDateTime lastModifiedTime;

    private MoneyTransaction(@NonNull Long id, @NonNull Account debit, @NonNull Account credit, @NonNull BigDecimal amount, @NonNull LocalDateTime transactionCreatedTime, LocalDateTime transactionLastModifiedTime) {

        Validator.validateAmountPositive(amount);
        Validator.validateAccountsAreValid(debit, credit);
        Validator.validateAccountIsDifferent(debit, credit);
        Validator.validateCurrencyIsTheSame(debit, credit);

        this.id = id;
        this.debit = debit;
        this.credit = credit;
        this.amount = amount;
        this.createdTime = transactionCreatedTime;
        this.lastModifiedTime = transactionLastModifiedTime;
        this.state = TransactionState.NEW;
    }

    private MoneyTransaction(@NonNull Long id) {
        this.id = id;
    }


    public static Transaction make(Long id, Account debit, Account credit, BigDecimal amount) {
        LocalDateTime now = LocalDateTime.now();
        return new MoneyTransaction(id, debit, credit, amount, now, now);
    }

    public static Transaction getFakeTransaction() {
        return new MoneyTransaction(FAKE_ID);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Account getDebit() {
        return debit;
    }

    @Override
    public Account getCredit() {
        return credit;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public LocalDateTime getLastModifiedTime() {
        return lastModifiedTime;
    }

    @Override
    public synchronized TransactionState getState() {
        return state;
    }

    @Override
    public synchronized boolean run() {
        if (state != TransactionState.COMPLETED) {
            changeState();
            return doRun();
        }
        return false;
    }

    private boolean doRun() {
        final var debitLock = debit.writeLock();
        try {
            if (debitLock.tryLock(ConfigStore.TRANSACTION_WAITING_LOCK_TIME, TimeUnit.MILLISECONDS)) {
                try {
                    final var creditLock = credit.writeLock();
                    if (creditLock.tryLock(ConfigStore.TRANSACTION_WAITING_LOCK_TIME, TimeUnit.MILLISECONDS)) {
                        try {
                            if (debit.debit(amount)) {
                                if (credit.credit(amount)) {
                                    state = TransactionState.COMPLETED;
                                    log.trace("Transaction {} completed", id);
                                    return true;
                                }
                            }
                            state = TransactionState.LOW_FUNDS;
                        } finally {
                            creditLock.unlock();
                        }
                    } else {
                        state = TransactionState.LOCKED;
                    }
                } finally {
                    debitLock.unlock();
                }
            } else {
                state = TransactionState.LOCKED;
            }
        } catch (Throwable e) {
            state = TransactionState.INTERNAL_ERROR;
            log.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private void changeState() {
        if (state == TransactionState.LOW_FUNDS || state == TransactionState.INTERNAL_ERROR) {
            state = TransactionState.RESTARTED;
        }
    }

    @Override
    public String toString() {
        final var sb = new StringBuilder("MoneyTransaction{");
        sb.append("id=").append(id);
        sb.append(", debit=").append(debit);
        sb.append(", credit=").append(credit);
        sb.append(", amount=").append(amount);
        sb.append(", state=").append(state);
        sb.append(", createdTime=").append(createdTime);
        sb.append(", lastModifiedTime=").append(lastModifiedTime);
        sb.append('}');
        return sb.toString();
    }
}
