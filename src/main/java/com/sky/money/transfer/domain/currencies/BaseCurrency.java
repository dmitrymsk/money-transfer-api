package com.sky.money.transfer.domain.currencies;

import com.sky.money.transfer.utils.validators.Validator;
import lombok.NonNull;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.sky.money.transfer.config.ConfigStore.DEFAULT_CURRENCY_ISO_CODE;

public class BaseCurrency implements Currency {

    private static final ConcurrentMap<String, Currency> CURRENCIES = new ConcurrentHashMap<>();

    private final String isoCode;

    private BaseCurrency(String isoCode) {
        this.isoCode = isoCode;
    }

    public static Currency valueOf(@NonNull String isoCode) {
        Validator.validateCurrencyCode(isoCode);
        final var code = isoCode.toUpperCase();
        var currency = CURRENCIES.get(code);
        if (currency == null) {
            currency = CURRENCIES.computeIfAbsent(code, (k) -> new BaseCurrency(code));
        }
        return currency;
    }


    public static Currency getDefault() {
        return valueOf(DEFAULT_CURRENCY_ISO_CODE);
    }

    @Override
    public boolean isCorrect() {
        return isoCode.length() == ISO_CODE_LENGTH;
    }

    @Override
    public String getISOCode() {
        return isoCode;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof BaseCurrency))
            return false;
        final var other = (BaseCurrency) o;
        if (!other.canEqual(this)) return false;
        return Objects.equals(this.isoCode, other.isoCode);
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BaseCurrency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isoCode);
    }
}
