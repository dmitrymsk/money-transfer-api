package com.sky.money.transfer.domain.core;

public interface Checkable {

    boolean isCorrect();

    default boolean isNotCorrect() {
        return !isCorrect();
    }
}
