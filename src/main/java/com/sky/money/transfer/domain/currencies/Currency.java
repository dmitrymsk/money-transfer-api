package com.sky.money.transfer.domain.currencies;

import com.sky.money.transfer.domain.core.Checkable;

public interface Currency extends Checkable {

    int ISO_CODE_LENGTH = 3;

    String getISOCode();
}
