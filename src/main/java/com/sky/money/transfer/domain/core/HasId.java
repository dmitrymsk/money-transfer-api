package com.sky.money.transfer.domain.core;


public interface HasId extends Checkable {

    long FAKE_ID = -1L;

    Long getId();

    @Override
    default boolean isCorrect() {
        return FAKE_ID != getId();
    }
}
