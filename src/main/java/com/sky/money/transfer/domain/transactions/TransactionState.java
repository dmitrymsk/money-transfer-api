package com.sky.money.transfer.domain.transactions;

public enum TransactionState {

    NEW,
    LOW_FUNDS,
    COMPLETED,
    INTERNAL_ERROR,
    LOCKED,
    RESTARTED
}
