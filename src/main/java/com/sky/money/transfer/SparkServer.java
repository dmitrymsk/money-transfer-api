package com.sky.money.transfer;


import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.service.BankService;
import com.sky.money.transfer.service.DefaultBankService;
import com.sky.money.transfer.utils.JsonUtils;
import com.sky.money.transfer.utils.dto.AccountData;
import com.sky.money.transfer.utils.dto.BankTransactionData;
import com.sky.money.transfer.utils.pagination.PaginationParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Spark;

import javax.servlet.http.HttpServletResponse;
import java.util.NoSuchElementException;

import static com.sky.money.transfer.config.ConfigStore.*;
import static spark.Spark.*;

final class SparkServer {


    private static final Logger logger = LoggerFactory.getLogger(SparkServer.class);

    private static final BankService bankService = DefaultBankService.getInstance();


    public static void main(String[] args) {
        startWithData();
    }

    static void start() {
        startSparkServer();
    }

    static void startWithData() {
        bankService.generateSampleData();
        startSparkServer();
    }

    static void stop() {
        Spark.stop();
        Spark.awaitStop();
    }


    private static void startSparkServer() {
        threadPool(ConfigStore.SERVER_THREAD_COUNT);
        port(ConfigStore.SERVER_PORT);
        after((req, res) -> res.type(CONTENT_TYPE));
        initRoutes();
        initExceptionsHandling();
        awaitInitialization();
    }


    private static void initRoutes() {

        path(SLASH + API_PREFIX + SLASH + API_VERSION, () -> {
            before(SLASH + "*", (req, res) -> logger.info("Received api call {}", req.pathInfo()));
            path(SLASH + ACCOUNTS_URI, () -> {
                get("", (req, res) -> {
                    final var pgParams = PaginationParams.from(req);
                    return JsonUtils.make().toJson(bankService.getAllAccounts(pgParams));
                });
                get(SLASH + ID_PARAM, (req, res) -> JsonUtils.make().toJson(bankService.getAccountById(getId(req))));
                get(SLASH + ID_PARAM + SLASH + TRANSACTIONS_URI, (req, res) -> {
                    final var pgParams = PaginationParams.from(req);
                    return JsonUtils.make().toJson(bankService.getTransactionsByAccount(getId(req), pgParams));
                });
                post("", (req, res) -> {
                    final var accountData = JsonUtils.make().fromJson(req.body(), AccountData.class);
                    final var account = bankService.createAccount(accountData.getHolderName(), accountData.getInitialBalance());
                    res.status(HttpServletResponse.SC_CREATED);
                    res.header("Location", SLASH + ACCOUNTS_URI + SLASH + account.getId());
                    return JsonUtils.make().toJson(account);
                });

            });
            path(SLASH + TRANSACTIONS_URI, () -> {
                get("", (req, res) -> {
                    final var pgParams = PaginationParams.from(req);
                    return JsonUtils.make().toJson(bankService.getAllTransactions(pgParams));
                });
                get(SLASH + ID_PARAM, (req, res) -> JsonUtils.make().toJson(bankService.getTransactionById(getId(req))));
                post("", (req, res) -> {
                    final var transactionData = JsonUtils.make().fromJson(req.body(), BankTransactionData.class);
                    final var transaction = bankService.createTransaction(transactionData);
                    res.status(HttpServletResponse.SC_CREATED);
                    res.header("Location", SLASH + TRANSACTIONS_URI + SLASH + transaction.getId());
                    return JsonUtils.make().toJson(transaction);
                });
            });
        });

    }


    private static void initExceptionsHandling() {
        exception(IllegalArgumentException.class, (e, req, res) ->
                fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));

        exception(NullPointerException.class, (e, req, res) ->
                fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));

        exception(NumberFormatException.class, (e, req, res) ->
                fillErrorInfo(res, e, HttpServletResponse.SC_BAD_REQUEST));

        exception(NoSuchElementException.class, (e, req, res) ->
                fillErrorInfo(res, e, HttpServletResponse.SC_NOT_FOUND));
    }


    private static void fillErrorInfo(Response res, Exception err, int errCode) {
        res.type(CONTENT_TYPE);
        res.status(errCode);
        res.body(JsonUtils.toJson(err, errCode));
    }


    private static Long getId(Request req) {
        return Long.valueOf(req.params("id"));
    }
}
