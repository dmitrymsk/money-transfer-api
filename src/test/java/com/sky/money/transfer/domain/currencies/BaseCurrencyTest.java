package com.sky.money.transfer.domain.currencies;

import com.sky.money.transfer.config.ConfigStore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BaseCurrencyTest {

    @Test
    void valueOf() {
        final Currency gbp = BaseCurrency.valueOf("GBP");
        assertNotNull(gbp);
        assertTrue(gbp.isCorrect());
        assertFalse(gbp.isNotCorrect());
        assertEquals("GBP", gbp.getISOCode());
        assertEquals(BaseCurrency.valueOf("GBP"), gbp);
        assertSame(BaseCurrency.valueOf("GBP"), gbp);

        final Currency usd = BaseCurrency.valueOf("USD");
        assertNotNull(usd);
        assertTrue(usd.isCorrect());
        assertFalse(usd.isNotCorrect());
        assertEquals("USD", usd.getISOCode());
        assertEquals(BaseCurrency.valueOf("USD"), usd);
        assertSame(BaseCurrency.valueOf("USD"), usd);

        assertNotEquals(gbp, usd);
    }

    @Test
    void defaultCurrency() {
        final Currency def = BaseCurrency.getDefault();
        assertNotNull(def);
        assertTrue(def.isCorrect());
        assertEquals(ConfigStore.DEFAULT_CURRENCY_ISO_CODE, def.getISOCode());
    }
}