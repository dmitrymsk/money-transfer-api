package com.sky.money.transfer.domain.accounts;

import com.sky.money.transfer.domain.currencies.BaseCurrency;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.sky.money.transfer.config.ConfigStore.ACCOUNT_NUMBER_DIGITS_COUNT;
import static com.sky.money.transfer.config.ConfigStore.DEFAULT_BANK_SORT_CODE;
import static org.junit.jupiter.api.Assertions.*;


class BankAccountTest {

    final BigDecimal DEFAULT_INITIAL_BALANCE = BigDecimal.TEN;
    final long DEFAULT_ACCOUNT_NUMBER = 15L;
    final String DEFAULT_HOLDER_NAME = "HolderName";

    @Test
    void getId() {
        final var a = makeDefaultAccount(999l);
        assertNotNull(a);
        assertTrue(a.isCorrect());
        assertFalse(a.isNotCorrect());
        assertEquals(999l, a.getId());
    }

    @Test
    void getFullAccountNumber() {
        final var a = makeDefaultAccount();
        assertNotNull(a);
        assertTrue(a.isCorrect());
        assertFalse(a.isNotCorrect());
        assertEquals(DEFAULT_BANK_SORT_CODE + " " +
                StringUtils.leftPad(String.valueOf(DEFAULT_ACCOUNT_NUMBER), ACCOUNT_NUMBER_DIGITS_COUNT, '0'), a.getFullAccountNumber());
    }

    @Test
    void getCurrency() {
        final var a = makeDefaultAccount();
        assertNotNull(a);
        assertTrue(a.isCorrect());
        assertFalse(a.isNotCorrect());
        assertEquals(BaseCurrency.getDefault(), a.getCurrency());


    }

    @Test
    void getBalance() {
        final var a = makeDefaultAccount();
        assertNotNull(a);
        assertTrue(a.isCorrect());
        assertFalse(a.isNotCorrect());
        assertEquals(DEFAULT_INITIAL_BALANCE, a.getBalance());
    }

    @Test
    void getHolder() {
        final var a = makeDefaultAccount();
        assertNotNull(a);
        assertTrue(a.isCorrect());
        assertFalse(a.isNotCorrect());
        assertEquals(DEFAULT_HOLDER_NAME, a.getHolderName());
    }

    @Test
    void debitAndCredit() {
        final var a = makeDefaultAccount();
        assertFalse(a.debit(BigDecimal.TEN.add(BigDecimal.ONE)));
        assertTrue(a.credit(BigDecimal.TEN));
        assertEquals(BigDecimal.valueOf(20), a.getBalance());
        assertTrue(a.debit(BigDecimal.ONE));
        assertEquals(BigDecimal.valueOf(19), a.getBalance());
    }


    private Account makeDefaultAccount() {
        return makeDefaultAccount(DEFAULT_ACCOUNT_NUMBER);
    }


    private Account makeDefaultAccount(Long id) {
        return BankAccount.createAccount(id, BaseCurrency.getDefault(), DEFAULT_BANK_SORT_CODE + ' ' +
                        StringUtils.leftPad(String.valueOf(DEFAULT_ACCOUNT_NUMBER),
                                ACCOUNT_NUMBER_DIGITS_COUNT, '0'),
                DEFAULT_HOLDER_NAME, DEFAULT_INITIAL_BALANCE);
    }
}