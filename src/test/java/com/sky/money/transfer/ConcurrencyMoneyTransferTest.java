package com.sky.money.transfer;

import com.sky.money.transfer.config.ConfigStore;
import com.sky.money.transfer.domain.transactions.TransactionState;
import com.sky.money.transfer.service.BankService;
import com.sky.money.transfer.service.DefaultBankService;
import com.sky.money.transfer.utils.dto.BankTransactionData;
import lombok.extern.slf4j.Slf4j;
import net.jodah.concurrentunit.Waiter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
public class ConcurrencyMoneyTransferTest {

    private static BankService bankService;
    private static ExecutorService executorService;
    private static Waiter waiter;
    private static BigDecimal initialBalance;
    private static CountDownLatch synchronizer;
    private volatile static boolean firstTransactionPassed;

    @BeforeAll
    public static void setUp() {

        bankService = DefaultBankService.getInstance();
        waiter = new Waiter();
        executorService = Executors.newFixedThreadPool(ConfigStore.TEST_COUNCURRENCY_NUM_OF_THREADS);
        initialBalance = BigDecimal.valueOf(ConfigStore.DEFAULT_INITIAL_BALANCE);

    }

    @AfterAll
    public static void tearDown() {
        executorService.shutdown();
    }

    @Test
    public void shouldHandleConcurrentTransactionsBetweenTwoPartiesAndKeepSameBalance() throws Exception {


        //given
        final var sender = bankService.createAccount("Rafa Nadal", initialBalance);
        final var receiver = bankService.createAccount("Rodger Federer", initialBalance);

        final var bankTransactionDataDirect = new BankTransactionData(sender.getId(), receiver.getId(), BigDecimal.ONE);
        final var bankTransactionDataReturn = new BankTransactionData(receiver.getId(), sender.getId(), BigDecimal.ONE);


        // when
        IntStream.range(0, ConfigStore.TEST_COUNCURRENCY_NUM_OF_TRANSACTIONS).forEach((int i) ->
        {
            executorService.submit(() -> createTransaction(bankTransactionDataDirect));
            executorService.submit(() -> createTransaction(bankTransactionDataReturn));
        });

        waiter.await(ConfigStore.TEST_COUNCURRENCY_MAX_TEST_WAITING_TIME, TimeUnit.MILLISECONDS, ConfigStore.TEST_COUNCURRENCY_NUM_OF_TRANSACTIONS * 2);

        // then
        waiter.assertEquals(initialBalance, sender.getBalance());
        waiter.assertEquals(initialBalance, receiver.getBalance());

    }

    @Test
    public void shouldHandleConcurrentTransactionAndNotAllowMakeBalanceBelowZero() throws Exception {


        //given
        final var receiver = bankService.createAccount("Rodger Federer", initialBalance);
        final var senderWithLowBalance = bankService.createAccount("Marin Cilic", BigDecimal.ONE);

        BankTransactionData bankTransactionData = new BankTransactionData(senderWithLowBalance.getId(), receiver.getId(), BigDecimal.ONE);

        synchronizer = new CountDownLatch(ConfigStore.TEST_COUNCURRENCY_NUM_OF_THREADS);

        // when

        IntStream.range(0, ConfigStore.TEST_COUNCURRENCY_NUM_OF_THREADS).forEach((int i) ->
                executorService.submit(() -> createTransactionWithLatch(bankTransactionData)));

        waiter.await(ConfigStore.TEST_COUNCURRENCY_MAX_TEST_WAITING_TIME, TimeUnit.MILLISECONDS, ConfigStore.TEST_COUNCURRENCY_NUM_OF_THREADS);

        // then

        waiter.assertTrue(senderWithLowBalance.getBalance().compareTo(BigDecimal.ZERO) >= 0);


    }

    private void createTransaction(BankTransactionData bankTransactionData) {
        var transaction = bankService.createTransaction(bankTransactionData);
        waiter.assertEquals(TransactionState.COMPLETED, transaction.getState());
        waiter.resume();
    }

    private void createTransactionWithLatch(BankTransactionData bankTransactionData) {
        synchronizer.countDown();
        try {
            synchronizer.await();
        } catch (InterruptedException e) {
            log.warn(e.getMessage(), e);
        }
        var transaction = bankService.createTransaction(bankTransactionData);
        if (!firstTransactionPassed) {
            firstTransactionPassed = true;
            waiter.assertEquals(TransactionState.COMPLETED, transaction.getState());
        } else
            waiter.assertEquals(TransactionState.LOW_FUNDS, transaction.getState());
        waiter.resume();

    }


}
