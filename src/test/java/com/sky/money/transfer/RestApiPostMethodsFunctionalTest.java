package com.sky.money.transfer;


import com.sky.money.transfer.domain.accounts.Account;
import com.sky.money.transfer.domain.accounts.BankAccount;
import com.sky.money.transfer.domain.transactions.TransactionState;
import com.sky.money.transfer.utils.dto.AccountData;
import com.sky.money.transfer.utils.dto.BankTransactionData;
import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.sky.money.transfer.config.ConfigStore.*;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;


public class RestApiPostMethodsFunctionalTest {
    private static Account accountOne;
    private static Account accountTwo;

    @BeforeAll
    static void setUp() {
        SparkServer.start();
        RestAssured.baseURI = "http://" + SERVER_HOST;
        RestAssured.port = SERVER_PORT;
        RestAssured.basePath = API_PREFIX + "/" + API_VERSION;

        final var accountDataOne = new AccountData("Michael Jordan", BigDecimal.TEN);
        accountOne = given()
                .body(accountDataOne)
                .when().post(ACCOUNTS_URI)
                .then().extract()
                .as(BankAccount.class);
        final var accountDataTwo = new AccountData("Kobe Bryant", BigDecimal.TEN);
        accountTwo = given()
                .body(accountDataTwo)
                .when().post(ACCOUNTS_URI)
                .then().extract()
                .as(BankAccount.class);

    }

    @AfterAll
    static void tearDown() {
        SparkServer.stop();
    }


    @Test
    public void shouldCreateWith201AndGetOneAccount() {

        final var accountData = new AccountData("Kevin Durant", BigDecimal.TEN);
        final var bankAccount = given()
                .body(accountData)
                .when()
                .post(ACCOUNTS_URI)
                .then()
                .statusCode(201)
                .extract()
                .as(BankAccount.class);
        Assertions.assertEquals(accountData.getHolderName(), bankAccount.getHolderName());
        Assertions.assertEquals(BigDecimal.TEN, bankAccount.getBalance());
    }


    @Test
    public void shouldNotCreateAccountWithNegativeBalanceAndReturn400() {
        final var accountData = new AccountData("Lebron James", BigDecimal.TEN.negate());
        given()
                .body(accountData)
                .when()
                .post(ACCOUNTS_URI)
                .then()
                .statusCode(400)
                .body("errorMessage", equalTo("Amount cannot be negative"));
    }

    @Test
    public void shouldRespondLowFundsOnCreateTransactionWithBiggerAmountForTransfer() {

        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.TEN.add(BigDecimal.ONE));

        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(201)
                .body("state", equalTo(TransactionState.LOW_FUNDS.toString()));


    }


    @Test
    public void shouldRespond201AndCompletedStateOnCreateTransactionWithCorrectParameters() {

        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.ONE);

        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(201)
                .body("state", equalTo(TransactionState.COMPLETED.toString()));
    }


    @Test
    public void shouldRespond201AndCorrectBalancesOnCreateTransactionWithCorrectParameters() {

        final var accountDataOne = new AccountData("Lamar Odom", BigDecimal.TEN);
        accountOne = given()
                .body(accountDataOne)
                .when().post(ACCOUNTS_URI)
                .then().extract()
                .as(BankAccount.class);
        final var accountDataTwo = new AccountData("Dennis Rodman", BigDecimal.TEN);
        accountTwo = given()
                .body(accountDataTwo)
                .when().post(ACCOUNTS_URI)
                .then().extract()
                .as(BankAccount.class);

        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.TEN.subtract(BigDecimal.ONE));

        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(201)
                .body("debit.balance", equalTo(BigDecimal.ONE.intValue()))
                .body("credit.balance", equalTo(BigDecimal.TEN.add(BigDecimal.TEN.subtract(BigDecimal.ONE)).intValue()));
    }


    @Test
    public void shouldRespond400OnTryingToSendToSameAccount() {

        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountOne.getId(), BigDecimal.ONE);

        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(400)
                .body("errorMessage", equalTo("Accounts must be different"));
    }


    @Test
    public void shouldRespond404OnTryingToSendToUnknownReceiver() {

        final var unknownReceiver = 123213123123L;
        final var bankTransactionData = new BankTransactionData(accountOne.getId(), unknownReceiver, BigDecimal.ONE);
        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(404)
                .body("errorMessage", equalTo("Account with id ".concat(String.valueOf(unknownReceiver)).concat(" is not found")));
    }

    @Test
    public void shouldRespond404OnTryingToSendFromUnknownSender() {

        final var unknownSender = 123213123123L;
        final var bankTransactionData = new BankTransactionData(unknownSender, accountTwo.getId(), BigDecimal.ONE);
        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(404)
                .body("errorMessage", equalTo("Account with id ".concat(String.valueOf(unknownSender)).concat(" is not found")));
    }

    @Test
    public void shouldRespond400OnTryingToSendNegativeAmount() {

        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.ONE.negate());
        given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(400)
                .body("errorMessage", equalTo("Amount must be greater than zero"));
    }


    @Test
    public void shouldBeFoundByIdAfterTransactionPerformed() {


        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.ONE);
        Integer transactionId = given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .extract().path("id");

        get(TRANSACTIONS_URI.concat("/").concat(transactionId.toString()))
                .then()
                .statusCode(200)
                .body("id", equalTo(transactionId));
    }


    @Test
    public void shouldBeRecordedInSenderAndReceiverTransactionsList() {


        final var bankTransactionData = new BankTransactionData(accountOne.getId(), accountTwo.getId(), BigDecimal.ONE);
        Integer transactionId = given()
                .body(bankTransactionData)
                .when()
                .post(TRANSACTIONS_URI)
                .then()
                .statusCode(201)
                .extract().path("id");

        given()
                .pathParam("id", accountOne.getId())
                .when().get(ACCOUNTS_URI.concat("/{id}").concat("/").concat(TRANSACTIONS_URI))
                .then()
                .statusCode(200)
                .body(containsString(transactionId.toString()));


        given()
                .pathParam("id", accountTwo.getId())
                .when().get(ACCOUNTS_URI.concat("/{id}").concat("/").concat(TRANSACTIONS_URI))
                .then()
                .statusCode(200)
                .body(containsString(transactionId.toString()));

    }


}
