package com.sky.money.transfer;


import io.restassured.RestAssured;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static com.sky.money.transfer.config.ConfigStore.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;


public class RestApiGetMethodsFunctionalTest {
    @BeforeAll
    static void setUp() {
        SparkServer.startWithData();
        RestAssured.baseURI = "http://".concat(SERVER_HOST);
        RestAssured.port = SERVER_PORT;
        RestAssured.basePath = API_PREFIX.concat("/").concat(API_VERSION);

    }

    @AfterAll
    static void tearDown() {
        SparkServer.stop();
    }


    @Test
    public void shouldRespondOKOnGetAllAccountsListWithNoParams() {
        given()
                .when().get(ACCOUNTS_URI)
                .then()
                .statusCode(200)
                .body(containsString("holderName"))
                .body("recordsPerPage", equalTo(100));
    }

    @Test
    public void shouldRespondOKOnGetAccountsListOWithPaginationParams() {
        given()
                .param("l", 20)
                .and()
                .param("p", 2)
                .when().get(ACCOUNTS_URI)
                .then()
                .statusCode(200)
                .body("pageNumber", equalTo(2))
                .body("recordsPerPage", equalTo(20));
    }

    @Test
    public void shouldRespond400OnAccountsListWithBadPaginationParams() {
        given()
                .param("l", 20)
                .and()
                .param("p", -4)
                .when().get(ACCOUNTS_URI)
                .then()
                .statusCode(400);
    }


    @Test
    public void shouldRespondOKOnGetAccountWithGivenId() {
        int accountId = 4;
        given()
                .pathParam("id", accountId)
                .when().get(ACCOUNTS_URI.concat("/{id}"))
                .then()
                .statusCode(200)
                .body("id", equalTo(4));
    }

    @Test
    public void shouldRespond404OnGetAccountWithUnknownId() {
        String unknownAccountId = "43123123311";
        given()
                .pathParam("id", unknownAccountId)
                .when().get(ACCOUNTS_URI.concat("/{id}"))
                .then()
                .statusCode(404)
                .body(containsString("Account with id ".concat(unknownAccountId).concat(" is not found")));
    }

    @Test
    public void shouldRespond400OnGetAccountWithBadId() {
        String badAccountId = "bad id";
        given()
                .pathParam("id", badAccountId)
                .when().get(ACCOUNTS_URI.concat("/{id}"))
                .then()
                .statusCode(400)
                .body("errorMessage", StringContains.containsString(badAccountId));
    }


    @Test
    public void shouldRespondOKOnGetAllTransactionsWithNoParams() {
        given()
                .when().get(TRANSACTIONS_URI)
                .then()
                .statusCode(200)
                .body("recordsPerPage", equalTo(100))  //default page size
                .body(containsString("\"state\": \"COMPLETED\""));
    }

    @Test
    public void shouldRespondOKOnGetTransactionWithGivenId() {
        int realTransactionId = 8;
        given()
                .pathParam("id", realTransactionId)
                .when().get(TRANSACTIONS_URI.concat("/{id}"))
                .then()
                .statusCode(200)
                .body("id", equalTo(realTransactionId));


    }

    @Test
    public void shouldRespond404OnGetTransactionWithUnknownId() {
        String unknownTransactionId = "43123123311";
        given()
                .pathParam("id", unknownTransactionId)
                .when().get(TRANSACTIONS_URI.concat("/{id}"))
                .then()
                .statusCode(404)
                .body("errorMessage", equalTo("Transaction with id "
                        + unknownTransactionId + " is not found"));

    }


    @Test
    public void shouldRespondOkOnGetAllTransactionsForGivenAccount() {
        int accountId = 4;
        given()
                .pathParam("id", accountId)
                .when().get(ACCOUNTS_URI.concat("/{id}").concat("/").concat(TRANSACTIONS_URI))
                .then()
                .statusCode(200)
                .body(containsString("\"id\": " + accountId));

    }

    @Test
    public void shouldRespondOkOnGetAllTransactionsForGivenAccountWithPagination() {
        int accountId = 4;
        given()
                .pathParam("id", accountId)
                .param("l", 5)
                .and()
                .param("p", 1)
                .when().get(ACCOUNTS_URI.concat("/{id}").concat("/").concat(TRANSACTIONS_URI))
                .then()
                .statusCode(200)
                .body(containsString("\"id\": " + accountId))
                .body("pageNumber", equalTo(1))
                .body("recordsPerPage", equalTo(5));

    }

}
