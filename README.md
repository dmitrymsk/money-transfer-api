##Simple money transfer api


###How to run
```
mvn clean install
java -jar  target/money-transfer-api-1.0.1.jar

```

###Sample api requests:
####Get some info about accounts:
- http://localhost:7001/api/v1/accounts
- http://localhost:7001/api/v1/accounts/5
- http://localhost:7001/api/v1/accounts/5/transactions

####Create new account:
Request:
```

curl -X "POST" "http://localhost:7001/api/v1/accounts" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "holderName": "Andy Murray",
  "initialBalance": "13.12"
}
```
Response:
```
HTTP/1.1 201 Created
Connection: close
Date: Wed, 29 May 2019 02:24:43 GMT
Location: /accounts/50001
Content-Type: application/json
Server: Jetty(9.4.z-SNAPSHOT)

{
  "id": 50001,
  "currencyCode": "GBP",
  "fullAccountNumber": "40-00-03 00050001",
  "holderName": "Andy Murray",
  "balance": 13.12
}
```

####Get some info about transactions:
- http://localhost:7001/api/v1/transactions
- http://localhost:7001/api/v1/transactions/10

####Make a money transfer:
Request:
```

curl -X "POST" "http://localhost:7001/api/v1/transactions" \
     -H 'Content-Type: application/json; charset=utf-8' \
     -d $'{
  "debitAccountId": "12",
  "creditAccountId": "13",
  "amount": "6.21"
}'
```
Response:

```
HTTP/1.1 201 Created
Connection: close
Date: Wed, 29 May 2019 02:26:16 GMT
Location: /transactions/100002
Content-Type: application/json
Server: Jetty(9.4.z-SNAPSHOT)

{
  "id": 100002,
  "debit": {
    "id": 12,
    "currencyCode": "GBP",
    "fullAccountNumber": "40-00-03 00000012",
    "holderName": "djvggnhrfd",
    "balance": 1008.30
  },
  "credit": {
    "id": 13,
    "currencyCode": "GBP",
    "fullAccountNumber": "40-00-03 00000013",
    "holderName": "imydtrycoo",
    "balance": 1009.27
  },
  "amount": 6.21,
  "state": "COMPLETED",
  "createdTime": {
    "date": {
      "year": 2019,
      "month": 5,
      "day": 29
    },
    "time": {
      "hour": 5,
      "minute": 26,
      "second": 16,
      "nano": 873632000
    }
  },
  "lastModifiedTime": {
    "date": {
      "year": 2019,
      "month": 5,
      "day": 29
    },
    "time": {
      "hour": 5,
      "minute": 26,
      "second": 16,
      "nano": 873632000
    }
  }
}
```
###Notes:
- based on http://sparkjava.com/
- built with java 12 
- limited multicurrency support (default currency - GBP)
- pagination support: http://localhost:7001/api/v1/transactions?p=2&l=20
- approx 30% unit tests code coverage 
- in-memory storage based on ConcurrentHashMap
- see myapp.conf for server and test data tuning
- a lot of sample accounts and transactions are generated while the server is starting







